import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

public class TreePanel extends JPanel {
	private int nodeWidth, nodeHeight;
	private Node tree;
	private int index;

	public TreePanel(int width, int height){
		setPreferredSize(new Dimension(width, height));
	}

	public void paintComponent(Graphics g){
		index = 0;
		if(tree != null)
			paintTree(g, tree, 0);
	}

	public Point paintTree(Graphics g, Node node, int level){
		if(node instanceof OperatorNode){
			OperatorNode opnode = (OperatorNode) node;
			Point leftpos, rightpos;

			leftpos = paintTree(g, opnode.getLeft(), level+1);

			int x = index * nodeWidth;
			int y = level * nodeHeight;
			String str = String.valueOf(opnode.getOperator());

			g.drawOval(x, y, nodeWidth, nodeHeight);
			g.drawString(str, x + nodeWidth / 2 - 2, y + nodeHeight / 2 - 2);

			index += 1;

			rightpos = paintTree(g, opnode.getRight(), level+1);

			g.drawLine(x, y + nodeHeight / 2, 
						leftpos.x + nodeWidth / 2, leftpos.y);
			g.drawLine(x + nodeWidth, y + nodeHeight / 2,
						rightpos.x + nodeWidth / 2, rightpos.y);

			return new Point(x, y);
		} 
		else {
			int x = index * nodeWidth;
			int y = level * nodeHeight;
			String str = String.valueOf(node.evaluate());

			g.drawOval(x, y, nodeWidth, nodeHeight);
			g.drawString(str, x + nodeWidth / 2 - 2, y + nodeHeight / 2 - 2);
			
			index += 1;

			return new Point(x, y);
		}
	}

	public void setTree(Node n){
		nodeWidth = getSize().width / n.size();
		nodeHeight = getSize().height / n.height();
		tree = n;
	}
}
