public interface Node {
	double evaluate();
	int height();
	int size();
}
