public class OperatorNode implements Node {
	private char operator;
	private Node left, right;
	
	public OperatorNode(char operator, Node left, Node right){
		this.operator = operator;
		this.left = left;
		this.right = right;
	}

	public double evaluate(){
		if(operator == '+')
			return left.evaluate() + right.evaluate();
		if(operator == '-')
			return left.evaluate() - right.evaluate();
		if(operator == '*')
			return left.evaluate() * right.evaluate();
		if(operator == '/')
			return left.evaluate() / right.evaluate();
		if(operator == '^')
			return Math.pow(left.evaluate(), right.evaluate());
		return 0;
	}

	public String toString(){
		return '(' + left.toString() + " " + operator + " " + right.toString() + ')';
	}

	public int height(){
		return 1 + Math.max(left.height(), right.height());
	}

	public int size(){
		return 1 + left.size() + right.size();
	}

	public char getOperator(){
		return operator;
	}

	public Node getLeft(){ 
		return left;
	}

	public Node getRight(){
		return right;
	}
}
