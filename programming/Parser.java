import java.util.LinkedList;

public class Parser {
	static char[] operators = {'+', '-', '/', '*', '^'};

	public static LinkedList<String> lex(String str){
		LinkedList<String> tokens = new LinkedList<String>();
		int start = 0;
		int i;
		char c;
		
		for(i=0; i<str.length(); i++){
			c = str.charAt(i);
			if(c == ' '){
				if(i > start) 
					tokens.add(str.substring(start, i));
				start = i+1;
			}
			
			else if(c == '-' && (tokens.isEmpty() || !isNumber(tokens.getLast()))){
				continue;	
			}

			else if(isOperator(c) || isBracket(c)){
				if(i > start)
					tokens.add(str.substring(start, i));
				tokens.add(String.valueOf(c));
				start = i+1;
			}
		}

		if(i > start)
			tokens.add(str.substring(start));

		return tokens;
	}

	public static void unwindOperator(LinkedList<String> output, 
			LinkedList<Character> stack, char op){
		int opprec = getPrecedence(op);
		
		while(!stack.isEmpty()){
			int topprec = getPrecedence(stack.getLast());
			
			if(topprec < opprec)
				break;
			
			output.add(stack.removeLast().toString());
		}

		stack.addLast(op);
	}

	public static void unwindParen(LinkedList<String> output, 
			LinkedList<Character> stack){
		
		while(!stack.isEmpty()){
			Character c = stack.removeLast();

			if(c == '(')
				break;

			output.add(c.toString());
		}
	}


	public static LinkedList<String> toPostfix(LinkedList<String> infix){
		LinkedList<String> postfix = new LinkedList<String>();
		LinkedList<Character> opstack = new LinkedList<Character>();

		for(String tok: infix){
			char c = tok.charAt(0);
			if(isOperator(tok)){
				if(opstack.isEmpty()){
					opstack.add(c);
				}
				else if(getPrecedence(c) > getPrecedence(opstack.getLast())){
					opstack.add(c);
				} else {
					unwindOperator(postfix, opstack, c);
				}
			} 
			else if(c == '('){
				opstack.add(c);
			} 
			else if(c == ')'){
				unwindParen(postfix, opstack);
			}
			else {
				postfix.add(tok);
			}
		}

		unwindParen(postfix, opstack);
		
		return postfix;
	}

	public static Node parseTree(LinkedList<String> tokens){
		LinkedList<Node> stack = new LinkedList<Node>();

		for(String tok: tokens){
			if(isOperator(tok)){
				Node right = stack.removeLast();
				Node left = stack.removeLast();

				Node opnode = new OperatorNode(tok.charAt(0), left, right);
				stack.addLast(opnode);
			} else {
				Node node = new NumberNode(Double.parseDouble(tok));
				stack.addLast(node);
			}
		}

		return stack.removeLast();
	}

	public static double evaluate(String expr){
		LinkedList<String> infix = lex(expr);
		LinkedList<String> postfix = toPostfix(infix);
		Node topnode = parseTree(postfix);
		return topnode.evaluate();
	}

	public static boolean isOperator(String str){
		return str.length() == 1 && isOperator(str.charAt(0));
	}

	public static boolean isOperator(char c){
		for(char op: operators){
			if(op == c) return true;
		}
		return false;
	}

	public static int getPrecedence(char op){
		if(op == '(')
			return -1;
		if(op == '+' || op == '-')
			return 0;
		if(op == '*' || op == '/')
			return 1;
		if(op == '^')
			return 2;
		return -1;
	}

	public static boolean isBracket(char c){
		return c == '(' || c == ')';
	}

	public static boolean isNumber(String str){
		return str.matches("[\\d.\\-]+");
	}
}
