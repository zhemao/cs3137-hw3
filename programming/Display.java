import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;

public class Display extends JPanel implements ActionListener {
	
	JTextField infixInput;
	JLabel postfixDisplay;
	JLabel resultDisplay;
	JLabel infixDisplay;
	LinkedList<String> postfixTokens;
	Node exprTree;
	TreePanel tpanel;

	public Display(){
		setPreferredSize(new Dimension(500, 500));
		
		infixInput = new JTextField();
		infixInput.setPreferredSize(new Dimension(450, 25));
		add(infixInput);

		add(makeButtonPanel());

		postfixDisplay = new JLabel();
		postfixDisplay.setPreferredSize(new Dimension(450, 25));
		add(postfixDisplay);

		tpanel = new TreePanel(475, 300);
		add(tpanel);

		resultDisplay = new JLabel("Result: ");
		resultDisplay.setPreferredSize(new Dimension(450, 25));
		add(resultDisplay);

		infixDisplay = new JLabel("Infix: ");
		infixDisplay.setPreferredSize(new Dimension(450, 25));
		add(infixDisplay);
	}

	private JPanel makeButtonPanel(){
		String[] commands = {"Postfix", "Tree", "Evaluate", "Infix"};
		JPanel panel = new JPanel();

		for(String str: commands){
			JButton button = new JButton(str);
			button.addActionListener(this);
			button.setActionCommand(str);
			panel.add(button);
		}

		return panel;
	}
	
	public static void main(String args[]){
		JFrame frame = new JFrame("Expression Tree");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new Display());
		frame.pack();
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e){
		if(e.getActionCommand().equals("Postfix")){
			LinkedList<String> infixTokens = Parser.lex(infixInput.getText());
			postfixTokens = Parser.toPostfix(infixTokens);

			String postfix = "";

			for(String tok: postfixTokens){
				postfix += tok + " ";
			}

			postfixDisplay.setText(postfix);
		}
		else if(e.getActionCommand().equals("Tree")){
			if(postfixTokens != null){
				exprTree = Parser.parseTree(postfixTokens);
				tpanel.setTree(exprTree);
				tpanel.repaint();
			}
		} 
		else if(e.getActionCommand().equals("Evaluate")){
			if(exprTree != null){
				double value = exprTree.evaluate();
				resultDisplay.setText("Result: " + value);
			}
		}
		else if(e.getActionCommand().equals("Infix")){
			if(exprTree != null){
				infixDisplay.setText("Infix: " + exprTree.toString());
			}
		}
	}
}
