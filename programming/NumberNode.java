public class NumberNode implements Node {
	private double number;

	public NumberNode(double number){
		this.number = number;
	}

	public double evaluate(){
		return number;
	}

	public int height(){
		return 1;
	}

	public int size(){
		return 1;
	}

	public String toString(){
		return String.valueOf(number);
	}
}
