import java.util.Scanner;
import java.util.LinkedList;

public class Tester{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.print("expr> ");
		while(input.hasNextLine()){
			String line = input.nextLine();

			if(line.equals("exit"))
				break;

			LinkedList<String> infix = Parser.lex(line);
			LinkedList<String> postfix = Parser.toPostfix(infix);

			System.out.print("infix: ");
			
			for(String tok: infix)
				System.out.print(tok + " ");
			
			System.out.print("\npostfix: ");
			for(String tok: postfix)
				System.out.print(tok + " ");

			Node n = Parser.parseTree(postfix);
			double d = n.evaluate();
			System.out.println("\n"+d);

			System.out.print("infix: ");
			System.out.println(n);

			System.out.println("height: " + n.height());
			System.out.println("size: " + n.size());
			
			System.out.print("expr> ");
		}
	}
}
