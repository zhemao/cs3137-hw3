#include "bintree.h"
#include "queue.h"
#include <stdlib.h>

void destroy_tree(node_p node){
	if(node->left != NULL){
		destroy_tree(node->left);
	}
	if(node->right != NULL){
		destroy_tree(node->right);
	}
	
	free(node->data);
	free(node);
}

node_p array_to_tree(char **arr, int i, int max){
	if(i >= max || arr[i] == NULL)
		return NULL;
	
	node_p node = (node_p)malloc(sizeof(struct node));
	node->data = arr[i];

	node->left = array_to_tree(arr, LEFT(i), max);
	node->right = array_to_tree(arr, RIGHT(i), max);

	return node;
}

void print_tree(FILE * f, node_p tree){
	queue_p q = create_queue(tree);
	node_p node;
	
	while(q != NULL){
		node = qhead(q);
		q = qpoll(q);

		if(node == NULL){
			fprintf(f, "\n");
		}
		else {
			fprintf(f, "%s\n", node->data);
			q = qpush(q, node->left);
			q = qpush(q, node->right);
		}
	}
}

void inorder_walk(node_p node){
	if(node != NULL){
		inorder_walk(node->left);
		printf("%s\n", node->data);
		inorder_walk(node->right);
	}
}
