#include "queue.h"
#include <stdlib.h>

queue_p create_queue(node_p node){
	queue_p q = (queue_p)malloc(sizeof(struct queue));
	q->node = node;
	q->next = q;

	return q;
}

node_p qhead(queue_p q){
	if(q == NULL)
		return NULL;
	return q->next->node;
}

queue_p qpush(queue_p q, node_p n){
	queue_p first;
	
	if(q == NULL)
		return create_queue(n);

	first = q->next;
	q->next = create_queue(n);
	q->next->next = first;

	return q->next;
}

queue_p qpoll(queue_p q){
	queue_p first;

	if(q == NULL)
		return NULL;
	if(q->next == q){
		free(q);
		return NULL;
	}

	first = q->next;
	q->next = first->next;

	free(first);
	
	return q;
}

void destroy_queue(queue_p q){
	while(q != NULL){
		q = qpoll(q);
	}
}
