#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "bintree.h"

struct queue {
	node_p node;
	struct queue * next;
};

typedef struct queue * queue_p;

queue_p create_queue(node_p node);
node_p qhead(queue_p q);
queue_p qpush(queue_p q, node_p n);
queue_p qpoll(queue_p q);
void destroy_queue(queue_p q);


#endif
