#include "bintree.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int chomp(char * str, int len){
	int i;
	char c;

	for(i=len-1; i>=0; i--){
		c = str[i];
		if(c != '\n' && c != ' ' && c != '\t'){
			str[i+1] = '\0';
			return i+1;
		}
	}

	return 0;
}

int main(int argc, char *argv[]){
	FILE * f;
	char *strarray[1024];
	char buf[1024];
	int i = 0;
	node_p tree;

	if(argc < 2){
		fprintf(stderr, "Usage: %s treefile\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	f = fopen(argv[1], "r");

	while(fgets(buf, 1024, f) && i < 1024){
		int len = strlen(buf);
		len = chomp(buf, len);

		if(len == 0)
			strarray[i] = NULL;
		else {
			strarray[i] = (char*)malloc(len+1);
			strncpy(strarray[i], buf, len+1);
		}
		i++;
	}
	
	tree = array_to_tree(strarray, 0, i);

	inorder_walk(tree);
	destroy_tree(tree);
	fclose(f);

	return 0;
}
