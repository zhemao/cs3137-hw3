#ifndef __BINTREE_H__
#define __BINTREE_H__

#include <stdio.h>

#define LEFT(n) (2 * n + 1)
#define RIGHT(n) (2 * n + 2)
#define PARENT(n) ((n - 1) / 2)

struct node {
	struct node * left;
	struct node * right;
	char * data;
};

typedef struct node * node_p;

void destroy_tree(node_p node);
node_p array_to_tree(char **arr, int i, int max);
void print_tree(FILE * f, node_p node);
void inorder_walk(node_p node);

#endif
